
insert ignore into sec_group(groupname, groupdesc) values('ADMIN','This group contains admins.');
insert ignore into sec_group(groupname, groupdesc) values('CUSTOMER','This group contains customers.');

insert ignore into sec_user(username, password) values('admin1',SHA2('admin1',256)); 
insert ignore into sec_user(username, password) values('admin2',SHA2('admin1',256)); 
insert ignore into sec_user(username, password) values('customer1',SHA2('customer1',256)); 
insert ignore into sec_user(username, password) values('customer2',SHA2('customer2',256)); 

insert ignore into sec_user_groups(username, groupname) values('admin1','ADMIN');
insert ignore into sec_user_groups(username, groupname) values('admin2','ADMIN');
insert ignore into sec_user_groups(username, groupname) values('customer1','CUSTOMER');
insert ignore into sec_user_groups(username, groupname) values('customer2','CUSTOMER');

insert into admins(firstname, lastname,email, designation, storeLocation, username) values ('Alice', 'Wonderland' , 'abc@abc.com', 'Owner', 'Napervile, Chicago','admin1');
insert into customer(firstname, middlename, lastname, email, gender, username,contactno) values ('Abc', 'Def', 'Xyz', 'd@abc.com', 'Male', 'customer1','3125648488');
insert into productcategory(categoryname,description) values ('Diary','All Diary products are listed here');
insert into productcategory(categoryname,description) values ('Meat','All Meat products are listed here');
insert into subcategory(categoryname,description,PRODUCTCATEGORY_CATEGORYID) values ('Milk','All Milk products are listed here',1);
insert into subcategory(categoryname,description,PRODUCTCATEGORY_CATEGORYID) values ('Yogurt','All Yogurt products are listed here',1);
insert into subcategory(categoryname,description,PRODUCTCATEGORY_CATEGORYID) values ('Dried Meat','All Dried Meat products are listed here',2);
insert into subcategory(categoryname,description,PRODUCTCATEGORY_CATEGORYID) values ('Halal Meat','All Halal Meat products are listed here',2);
insert into product(price,productname,qty,PRODUCTCATEGORY_CATEGORYID,SUBCATEGORY_CATEGORYID) values(3,'Fat free Diary Farm',50,1,1);
insert into product(price,productname,qty,PRODUCTCATEGORY_CATEGORYID,SUBCATEGORY_CATEGORYID) values(6,'Fat free Danon Curd',25,1,2);