/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.gsambasivan.ejb;

import edu.iit.sat.itmd4515.gsambasivan.domain.ProductCategory;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;


/**
 *
 * @author Swathi
 */
@Named
@Stateless
public class ProductCategoryService extends BaseService<ProductCategory> {

    public ProductCategoryService() {
        super(ProductCategory.class);
    }
    
public ProductCategory findByName(String categoryName) {
        return getEntityManager()
                .createNamedQuery("ProductCategory.findByName", ProductCategory.class)
                .setParameter("categoryName", categoryName)
                .getSingleResult();
    }
    @Override
    public List<ProductCategory> findAll() {
         return getEntityManager().createNamedQuery("ProductCategory.findAll", ProductCategory.class).getResultList();
    }
    
}
