/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.gsambasivan.web;

import edu.iit.sat.itmd4515.gsambasivan.domain.Product;
import edu.iit.sat.itmd4515.gsambasivan.domain.ProductCategory;
import edu.iit.sat.itmd4515.gsambasivan.domain.SubCategory;
import edu.iit.sat.itmd4515.gsambasivan.ejb.ProductService;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Swathi
 */
@ManagedBean
@ViewScoped
public class EditProductController {
    private long productId;
@EJB
    private ProductService productService;
    
    private String editName;
    private int editQty;
    private double editPrice;
    private ProductCategory productCategory;
    private SubCategory subCategory;

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public SubCategory getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
    }
    
   private Product p;
    public String getEditName() {
        return editName;
    }

    public void setEditName(String editName) {
        this.editName = editName;
    }

    public int getEditQty() {
        return editQty;
    }

    public void setEditQty(int editQty) {
        this.editQty = editQty;
    }

    public double getEditPrice() {
        return editPrice;
    }

    public void setEditPrice(double editPrice) {
        this.editPrice = editPrice;
    }
    
    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }
    /**
     * For edit setting old values in textbox
     * Getting details of the table row by passing through parameter
     */
    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        long selectedEntityId = Long.parseLong(params.get("productSel"));

        p = productService.findById(selectedEntityId);
        setEditName(p.getProductName());
        setEditPrice(p.getPrice());
        setEditQty(p.getQty());
        setProductCategory(p.getProductCategory());
        setSubCategory(p.getSubCategory());
    }
    /**
     * Edits are saved to database on save click
     */
    public void saveChanges(){
       
          p.setProductId(getProductId());
          p.setPrice(getEditPrice());
          p.setQty(getEditQty());
          p.setProductName(getEditName());
          productService.update(p);
          
    }
}
