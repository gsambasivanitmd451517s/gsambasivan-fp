/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.gsambasivan.web;

import edu.iit.sat.itmd4515.gsambasivan.domain.Customer;
import edu.iit.sat.itmd4515.gsambasivan.domain.Order;
import edu.iit.sat.itmd4515.gsambasivan.domain.security.User;
import edu.iit.sat.itmd4515.gsambasivan.ejb.CustomerService;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author Swathi
 */
@ManagedBean
public class RegisterController {
    @EJB
    private CustomerService customerService;
    private String firstName;
    private String middleName;
    private String lastName;
     private String email;
    private String gender;
    private String contactNo;
    private String username;
    private String password;
 

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    
    
    public RegisterController() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }
    
    public String doRegister(){
    List<Order> order = null ;
    Customer cust = new Customer();
    User user = new User();
    user.setUserName(username);
    user.setPassword(password);
    cust.setContactNo(contactNo);
    cust.setEmail(email);
    cust.setFirstName(firstName);
    cust.setGender(gender);
    cust.setLastName(lastName);
    cust.setMiddleName(middleName);
    cust.setUser(user);
    cust.setOrder(order);
    customerService.create(cust);
    addMessage("Added!", "New user Added successfully");
   return "/login.xhtml?faces-redirect=true";
    
    }
     public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
