/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.gsambasivan.web;

/**
 *
 * @author Swathi
 */

import edu.iit.sat.itmd4515.gsambasivan.domain.Product;
import edu.iit.sat.itmd4515.gsambasivan.domain.ProductCategory;
import edu.iit.sat.itmd4515.gsambasivan.domain.SubCategory;
import edu.iit.sat.itmd4515.gsambasivan.ejb.ProductCategoryService;
import edu.iit.sat.itmd4515.gsambasivan.ejb.ProductService;
import edu.iit.sat.itmd4515.gsambasivan.ejb.SubCategoryService;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
@ManagedBean
@ViewScoped
public class ProductController implements Serializable {
    @EJB
    private ProductService productService;
    
    private Product selectedProduct;
    private LazyDataModel<Product> pr;
    public void onRowSelect(SelectEvent event) {
        FacesMessage msg = new FacesMessage("Product Selected: ", ((Product) event.getObject()).getProductName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }
    
    public LazyDataModel<Product> getPr() {
        return pr;
    }

    public Product getSelectedProduct() {
        return selectedProduct;
    }

    public void setSelectedProduct(Product selectedProduct) {
        this.selectedProduct = selectedProduct;
    }
    
    @PostConstruct
    public void init() {
         
       pr =new ProductDataTableModel(productService.findAll());// productService.findAll(); 
    }
   
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    public String deleteProduct(Product p) throws IOException{
        
    productService.remove(p);
    System.out.println("Delete Selected......"+p.getProductName());
    addMessage("Successful", "Product has been deleted.Cheers!");
    return "/admin/welcome.xhtml?faces-redirect=true";
    }
      
}
