/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.gsambasivan.web;
import edu.iit.sat.itmd4515.gsambasivan.domain.Product;
import java.util.Comparator;
import org.primefaces.model.SortOrder;
/**
 *
 * @author Swathi
 */
public class LazySorter implements Comparator<Product>{
  private String sortField;
     
    private SortOrder sortOrder;
     
    public LazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }
 
  @Override
    public int compare(Product car1, Product car2) {
        try {
         
            Object value1 = Product.class.getField(this.sortField).get(car1);
            Object value2 = Product.class.getField(this.sortField).get(car2);
 
            int value = ((Comparable)value1).compareTo(value2);
             
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }   
}
