/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.gsambasivan.ejb;

import edu.iit.sat.itmd4515.gsambasivan.domain.SubCategory;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author Swathi
 */
@Named
@Stateless
public class SubCategoryService extends BaseService<SubCategory> {

    public SubCategoryService() {
        super(SubCategory.class);
    }
public List<SubCategory> findSubCategoryNames(String categoryName) {
         return getEntityManager().createNamedQuery("SubCategory.findSubCategoryNames", SubCategory.class)
                 .setParameter("categoryName", categoryName)
                 .getResultList();
    }

public SubCategory findByName(String categoryName) {
        return getEntityManager()
                .createNamedQuery("SubCategory.findByName", SubCategory.class)
                .setParameter("categoryName", categoryName)
                .getSingleResult();
    }
    @Override
    public List<SubCategory> findAll() {
         return getEntityManager().createNamedQuery("SubCategory.findAll", SubCategory.class).getResultList();
    }
    
}
