/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.gsambasivan.web;

import edu.iit.sat.itmd4515.gsambasivan.domain.Product;
import edu.iit.sat.itmd4515.gsambasivan.ejb.ProductService;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Swathi
 */
@ManagedBean
@ViewScoped
public class CustomerProductController {
    @EJB
    private ProductService productService;
    private List<Product> pr;

    public List<Product> getPr() {
        return pr;
    }

    public void setPr(List<Product> pr) {
        this.pr = pr;
    }
    /**
     * Initializing all the products to display in the grid view
     */
     @PostConstruct
    public void init() {
    
       pr = productService.findAll(); 
       
        
    }
   
}
