/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.gsambasivan.web;

import edu.iit.sat.itmd4515.gsambasivan.domain.Product;
import edu.iit.sat.itmd4515.gsambasivan.domain.ProductCategory;
import edu.iit.sat.itmd4515.gsambasivan.domain.SubCategory;
import edu.iit.sat.itmd4515.gsambasivan.ejb.ProductCategoryService;
import edu.iit.sat.itmd4515.gsambasivan.ejb.ProductService;
import edu.iit.sat.itmd4515.gsambasivan.ejb.SubCategoryService;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Swathi
 */

@ManagedBean
@ViewScoped
public class AddProductController implements Serializable {
      @EJB
    private SubCategoryService subCategoryService;
   @EJB
    private ProductCategoryService productCategoryService;
    @EJB
    private ProductService productService;
    private String productCategory;
  
  
    private String subCategory;
    
   private Map<String,Map<String,String>> data = new HashMap<String, Map<String,String>>();

    public Map<String, Map<String, String>> getData() {
        return data;
    }

    public void setData(Map<String, Map<String, String>> data) {
        this.data = data;
    }
    
    private Map<String,String> productCategories; 
    
       private Map<String,String> subCategories; 

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    

    public Map<String, String> getProductCategories() {
        return productCategories;
    }

    public void setProductCategories(Map<String, String> productCategories) {
        this.productCategories = productCategories;
    }

    public Map<String, String> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(Map<String, String> subCategories) {
        this.subCategories = subCategories;
    }
    /**
     * Initializing product category dropdown for add product button
     */
    @PostConstruct
    public void init() {
    productCategories = new HashMap<String, String>();
        for(ProductCategory proCat :productCategoryService.findAll()){
            productCategories.put(proCat.getCategoryName(),proCat.getCategoryName());
    }
        
    }
    /**
     * Initializing product sub-category dropdown for add product button
     */
    public void onProductCategoryChange(){
   Map<String,String> map = new HashMap<String, String>();
       for(SubCategory cat :subCategoryService.findSubCategoryNames(getProductCategory())){
            
            map.put(cat.getCategoryName(),cat.getCategoryName());
       }
         data.put(getProductCategory(), map);
         if(productCategory !=null && !productCategory.equals(""))
         {
             subCategories = data.get(productCategory);
          
         } // subCategories = data.get(productCategory);
        else
            subCategories = new HashMap<String, String>();
   
      
    }
   
     /**
      * Will persist the new product to the database
      * @param name
      * @param price
      * @param qty
      * @return 
      */
   
    public String addNewProduct(String name, String price, String qty){
    Product product = new Product();
      
        
        ProductCategory p = productCategoryService.findByName(getProductCategory());
        SubCategory s = subCategoryService.findByName(getSubCategory());
       product.setProductName(name);
       product.setPrice(Double.parseDouble(price));
       product.setQty(Integer.parseInt(qty));
      product.setSubCategory(s);
       product.setProductCategory(p);
       
       productService.create(product);
      
       return "/admin/welcome.xhtml?faces-redirect=true";
    }
}
