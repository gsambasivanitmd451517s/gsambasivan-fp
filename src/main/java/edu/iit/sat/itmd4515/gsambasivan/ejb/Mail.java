/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.gsambasivan.ejb;



import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.bean.RequestScoped;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Swathi
 */

@ManagedBean(name="mail")
@RequestScoped
public class Mail {

    /**
     * @param args the command line arguments
     */
      // TODO code application logic here
    private String header;
    private String emailAddr;
    private String name;
    private String body;
 
    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getEmailAddr() {
        return emailAddr;
    }

    public void setEmailAddr(String emailAddr) {
        this.emailAddr = emailAddr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
    public void openDialog() {
        Map<String,Object> options = new HashMap<String, Object>();
        options.put("resizable", false);
        options.put("draggable", false);
        options.put("modal", true);
        RequestContext.getCurrentInstance().openDialog("mail", options, null);
    }
    public void sendMail(){
    final String username = "grocery.gomi@gmail.com";
    final String password = "gomi1234";
    Properties properties = new Properties();
    properties.put("mail.smtp.auth","true");
    properties.put("mail.smtp.starttls.enable","true");
    properties.put("mail.smtp.host","smtp.gmail.com");
    properties.put("mail.smtp.ssl.trust","smtp.gmail.com");
    properties.put("mail.smtp.port","587");
    
    Session session = Session.getInstance(properties, new Authenticator() {
        protected PasswordAuthentication  getPasswordAuthentication(){
        return new PasswordAuthentication(username, password);
        }
});
    
   
        try {
             Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("grocery.gomi@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailAddr));
            message.setHeader("X-Mailer", "JavaMail");
            message.setSubject(header);
            message.setText("Dear"+name+",\n You have received the following message\n"+body);
            Transport.send(message);
            //System.out.println("Dear"+name+",\n You have received the following message/n"+body);
        } catch (MessagingException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);
        }
        RequestContext.getCurrentInstance().closeDialog(properties);
    }
}

