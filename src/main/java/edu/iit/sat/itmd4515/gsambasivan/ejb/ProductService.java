/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.gsambasivan.ejb;

import edu.iit.sat.itmd4515.gsambasivan.domain.Product;
import edu.iit.sat.itmd4515.gsambasivan.domain.SubCategory;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author Swathi
 */
@Named
@Stateless
public class ProductService extends BaseService<Product> {
    public ProductService() {
        super(Product.class);
    }
    public Product findById(Long id) {
        return getEntityManager()
                .createNamedQuery("Product.findByProductId", Product.class)
                .setParameter("productId", id)
                .getSingleResult();
    }
    @Override
    public List<Product> findAll() {
         return getEntityManager().createNamedQuery("Product.findAll", Product.class).getResultList();
    }
}
