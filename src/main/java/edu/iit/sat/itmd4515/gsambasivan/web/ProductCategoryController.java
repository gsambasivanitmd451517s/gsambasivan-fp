/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.gsambasivan.web;


import edu.iit.sat.itmd4515.gsambasivan.domain.ProductCategory;
import edu.iit.sat.itmd4515.gsambasivan.ejb.ProductCategoryService;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Swathi
 */
@ManagedBean
public class ProductCategoryController {

    @EJB
    private ProductCategoryService productCategoryService;

    public ProductCategoryController() {
    }
    /**
     * Modal popup to add Product Category
     */
    public void addProductCategoryDialog() {
        Map<String,Object> options = new HashMap<String, Object>();
        options.put("resizable", false);
        options.put("draggable", false);
        options.put("modal", true);
        RequestContext.getCurrentInstance().openDialog("addProductCategory", options, null);
    }
    /**
     * Adding Product category to persist in database
     * @param name
     * @param desc 
     */
     public void addProductCategory(String name, String desc){
       ProductCategory cat = new ProductCategory();
       
        cat.setCategoryName(name);
        cat.setDescription(desc);
        productCategoryService.create(cat);
        System.out.println("Product added");
        FacesContext context = FacesContext.getCurrentInstance();
         
        context.addMessage(null, new FacesMessage("Successful",  cat.getCategoryName()+" is added successfully") );
        RequestContext.getCurrentInstance().closeDialog(cat);
         
        
    }
}
