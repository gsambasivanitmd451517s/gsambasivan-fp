/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.gsambasivan.web;

import edu.iit.sat.itmd4515.gsambasivan.domain.ProductCategory;
import edu.iit.sat.itmd4515.gsambasivan.domain.SubCategory;
import edu.iit.sat.itmd4515.gsambasivan.ejb.ProductCategoryService;
import edu.iit.sat.itmd4515.gsambasivan.ejb.SubCategoryService;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Swathi
 */
@ManagedBean
public class SubCategoryController {
@EJB
    private SubCategoryService subCategoryService;
   @EJB
    private ProductCategoryService productCategoryService;
    
    private String productCategory;
    
    private Map<String,String> productCategories; 
@PostConstruct
    public void init() {
    productCategories = new HashMap<String, String>();
        for(ProductCategory proCat :productCategoryService.findAll()){
            productCategories.put(proCat.getCategoryName(),proCat.getCategoryName());
    }
        
    }
    public Map<String, String> getProductCategories() {
        return productCategories;
    }

    public void setProductCategories(Map<String, String> productCategories) {
        this.productCategories = productCategories;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

public SubCategoryController() {
    }

 public void addProductCategoryDialog() {
        Map<String,Object> options = new HashMap<String, Object>();
        options.put("resizable", false);
        options.put("draggable", false);
        options.put("modal", true);
        RequestContext.getCurrentInstance().openDialog("addSubCategory", options, null);
    }
  public void addSubCategory( String name, String desc){
       
      SubCategory cat = new SubCategory();
        
        ProductCategory p = productCategoryService.findByName(getProductCategory());
        cat.setCategoryName(name);
        cat.setDescription(desc);
       cat.setProductCategory(p);
              //  p.setCategoryName(getProductCategory());
        System.out.println(cat.getProductCategory().getCategoryId());
        subCategoryService.create(cat);
        System.out.println("Sub added");
        FacesContext context = FacesContext.getCurrentInstance();
         
        context.addMessage(null, new FacesMessage("Successful",  cat.getCategoryName()+" is added successfully") );
        RequestContext.getCurrentInstance().closeDialog(cat);
            
}
}